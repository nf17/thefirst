import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="https://jalin.co.id/">
                PT. Jalin Pembayaran Nusantara
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

const CssTextField = withStyles({
    root: {
        '& .MuiOutlinedInput-root': {
            '& fieldset': {
                borderColor: 'grey',
            },
            '&.Mui-focused fieldset': {
                borderColor: 'grey',
            },
        },
    },
})(TextField);
const red = '#E41B13';

const ColorButton = withStyles(theme => ({
    root: {
        color: theme.palette.getContrastText(red),
        backgroundColor: red,
        '&:hover': {
            backgroundColor: red,
        },
    },
}))(Button);
const useStyles = makeStyles(theme => ({
    card: {
        marginTop: theme.spacing(8),
        padding: '2rem'
    },
    paper: {
        marginTop: theme.spacing(1),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.common.primary,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));


export default function Login() {
    const classes = useStyles();

    return (
        <Container component="main" maxWidth="xs">
            <Card className={classes.card}>
                <div className={classes.paper}>
                    <Typography component="h1" variant="h5" style={{ color: '#E41B13', fontWeight: '900' }}>
                        Login to Jalin
                    </Typography>
                    <form className={classes.form} noValidate>

                        <CssTextField
                            className={classes.margin}
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="Email Address"
                            name="email"
                            autoComplete="email"
                            autoFocus
                        />
                        <CssTextField
                            className={classes.margin}
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Password"
                            type="password"
                            id="password"
                            autoComplete="current-password"
                        />
                        <FormControlLabel
                            control={<Checkbox value="remember" color="primary" />}
                            label="Remember me"
                        />
                        <ColorButton
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            href="/admin"
                        >
                            Login
                        </ColorButton>
                        <Grid container>
                            <Grid item>
                                <Link href="/register/register" variant="body2">
                                    Don't have an account? Sign Up
                            </Link>
                            </Grid>
                        </Grid>
                    </form>
                </div>
            </Card>
            <Box mt={8}>
                <Copyright />
            </Box>
        </Container>
    );
}