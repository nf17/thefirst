import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import MaterialTable from 'material-table';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import ResetIcon from '@material-ui/icons/Restore';
import SearchIcon from '@material-ui/icons/Search';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import Tooltip from '@material-ui/core/Tooltip';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import MenuItem from '@material-ui/core/MenuItem';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace';
const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "rgba(255,255,255,.62)",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#FFFFFF"
        }
    },
    cardfieldWhite: {
        color: "#FFFFFF",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    tableUpgradeWrapper: {
        display: "block",
        width: "100%",
        overflowX: "auto",
        WebkitOverflowScrolling: "touch",
        MsOverflowStyle: "-ms-autohiding-scrollbar"
    },
    table: {
        width: "100%",
        maxWidth: "100%",
        marginBottom: "1rem",
        backgroundColor: "transparent",
        borderCollapse: "collapse",
        display: "table",
        borderSpacing: "2px",
        borderColor: "grey",
        "& thdead tr th": {
            fontSize: "1.063rem",
            padding: "12px 8px",
            verticalAlign: "middle",
            fontWeight: "300",
            borderTopWidth: "0",
            borderBottom: "1px solid rgba(0, 0, 0, 0.06)",
            textAlign: "inherit"
        },
        "& tbody tr td": {
            padding: "12px 8px",
            verticalAlign: "middle",
            borderTop: "1px solid rgba(0, 0, 0, 0.06)"
        },
        "& td, & th": {
            display: "table-cell"
        }
    },
    center: {
        textAlign: "center"
    }
};

const currencies = [
    {
        value: '2019-Retrival request tidak di penuhi dalam batas waktu yang ditentukan',
        label: '2019-Retrival request tidak di penuhi dalam batas waktu yang ditentukan',
    },
    {
        value: '2019-Retrival request tidak di penuhi dalam batas waktu yang ditentukan',
        label: '2019-Retrival request tidak di penuhi dalam batas waktu yang ditentukan',
    },
    {
        value: '2019-Retrival request tidak di penuhi dalam batas waktu yang ditentukan',
        label: '2019-Retrival request tidak di penuhi dalam batas waktu yang ditentukan',
    },
    {
        value: '2019-Retrival request tidak di penuhi dalam batas waktu yang ditentukan',
        label: '2019-Retrival request tidak di penuhi dalam batas waktu yang ditentukan',
    },
];
const useStyles = makeStyles(styles);
export default function ViewDebitClaim() {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const descriptionElementRef = React.useRef(null);
    React.useEffect(() => {
        if (open) {
            const { current: descriptionElement } = descriptionElementRef;
            if (descriptionElement !== null) {
                descriptionElement.focus();
            }
        }
    }, [open]);

    return (
        <div>
            <Card>
                <CardBody>
                    <Card>
                        <DialogTitle id="scroll-dialog-title">Transaction Information</DialogTitle>
                        <CardBody >
                            <TextField
                                style={{ margin: '0.5rem' }}
                                disabled
                                variant="outlined"
                                id="standard-disabled"
                                label="Tanggal Transaksi"
                                defaultValue="2019-01-01"
                                className={classes.textField}
                                margin="dense"
                            />
                            <TextField
                                style={{ margin: '0.5rem' }}
                                disabled
                                variant="outlined"
                                id="standard-disabled"
                                label="Waktu Transaksi"
                                defaultValue="21:00:09"
                                className={classes.textField}
                                margin="dense"
                            />
                            <TextField
                                style={{ margin: '0.5rem' }}
                                disabled
                                variant="outlined"
                                id="standard-disabled"
                                label="Nomor Kartu"
                                defaultValue="201909438509458"
                                className={classes.textField}
                                margin="dense"
                            />
                            <TextField
                                style={{ margin: '0.5rem' }}
                                disabled
                                variant="outlined"
                                id="standard-disabled"
                                label="Nomor Transaksi"
                                defaultValue="200,900.00"
                                className={classes.textField}
                                margin="dense"
                            />
                            <TextField
                                style={{ margin: '0.5rem' }}
                                disabled
                                variant="outlined"
                                id="standard-disabled"
                                label="Status Transaksi"
                                defaultValue="Berhasil"
                                className={classes.textField}
                                margin="dense"
                            />
                            <TextField
                                style={{ margin: '0.5rem' }}
                                disabled
                                variant="outlined"
                                id="standard-disabled"
                                label="Kode Terminal"
                                defaultValue="103924789"
                                className={classes.textField}
                                margin="dense"
                            />
                            <TextField
                                style={{ margin: '0.5rem' }}
                                disabled
                                variant="outlined"
                                id="standard-disabled"
                                label="Kode Pemrosesan"
                                defaultValue="0000000000"
                                className={classes.textField}
                                margin="dense"
                            />
                            <TextField
                                style={{ margin: '0.5rem' }}
                                disabled
                                variant="outlined"
                                id="standard-disabled"
                                label="Kode Respons"
                                defaultValue="00"
                                className={classes.textField}
                                margin="dense"
                            />
                            <TextField
                                style={{ margin: '0.5rem' }}
                                disabled
                                variant="outlined"
                                id="standard-disabled"
                                label="Issuer"
                                defaultValue="MDR"
                                className={classes.textField}
                                margin="dense"
                            />
                            <TextField
                                style={{ margin: '0.5rem' }}
                                disabled
                                variant="outlined"
                                id="standard-disabled"
                                label="Acquirer"
                                defaultValue="BRI"
                                className={classes.textField}
                                margin="dense"
                            />
                            <TextField
                                style={{ margin: '0.5rem' }}
                                disabled
                                variant="outlined"
                                id="standard-disabled"
                                label="Nama Merchant"
                                defaultValue="Mitra Adira Utama"
                                className={classes.textField}
                                margin="dense"
                            />
                            <TextField
                                style={{ margin: '0.5rem' }}
                                disabled
                                variant="outlined"
                                id="standard-disabled"
                                label="Lokasi Merchant"
                                defaultValue="Pergudangan CID 87"
                                className={classes.textField}
                                margin="dense"
                            />
                            <TextField
                                style={{ margin: '0.5rem' }}
                                disabled
                                variant="outlined"
                                id="standard-disabled"
                                label="Nomor Referensi"
                                defaultValue="09897876867868"
                                className={classes.textField}
                                margin="dense"
                            />
                        </CardBody>
                    </Card>
                    <DialogTitle id="scroll-dialog-title">Claim History</DialogTitle>
                    <Card>
                        <DialogTitle id="scroll-dialog-title">Chargeback</DialogTitle>
                        <CardBody style={{ textAlign: 'center' }}>
                            <TextField
                                style={{ margin: '0.5rem' }}
                                disabled
                                variant="outlined"
                                id="standard-disabled"
                                label="Tanggal Claim"
                                defaultValue="2019-01-01"
                                className={classes.textField}
                                margin="dense"
                            />
                            <TextField
                                style={{ margin: '0.5rem' }}
                                disabled
                                variant="outlined"
                                id="standard-disabled"
                                label="No Registrasi Claim"
                                defaultValue="FCB20987657435"
                                className={classes.textField}
                                margin="dense"
                            />
                            <TextField
                                style={{ margin: '0.5rem' }}
                                disabled
                                variant="outlined"
                                id="standard-disabled"
                                label="Jenis Nominal Claim"
                                defaultValue="Full Amount"
                                className={classes.textField}
                                margin="dense"
                            />
                            <TextField
                                style={{ margin: '0.5rem' }}
                                disabled
                                variant="outlined"
                                id="standard-disabled"
                                label="Nominal Claim"
                                defaultValue="409,098.00"
                                className={classes.textField}
                                margin="dense"
                            />
                            <TextField
                                style={{ margin: '0.5rem' }}
                                disabled
                                variant="outlined"
                                id="standard-disabled"
                                label="Reason Code"
                                defaultValue="2019-Retrival request tidak di penuhi dalam batas waktu yang ditentukan"
                                className={classes.textField}
                                margin="dense"
                            />
                            <TextField
                                style={{ margin: '0.5rem' }}
                                disabled
                                variant="outlined"
                                id="standard-disabled"
                                label="Status Claim"
                                defaultValue="New 2019-09-09 operator@mmdk.com"
                                className={classes.textField}
                                margin="dense"
                            />
                            <TextField
                                style={{ margin: '0.5rem' }}
                                disabled
                                variant="outlined"
                                id="standard-disabled"
                                label="Pembuat Claim"
                                defaultValue="MDR"
                                className={classes.textField}
                                margin="dense"
                            />
                            <TextField
                                style={{ margin: '0.5rem' }}
                                disabled
                                variant="outlined"
                                id="standard-disabled"
                                label="Penerima Claim"
                                defaultValue="BRI"
                                className={classes.textField}
                                margin="dense"
                            />
                            <TextField
                                disabled
                                id="outlined-multiline-static"
                                label="Deskripsi"
                                multiline
                                defaultValue=""
                                fullWidth
                                rows="4"
                                className={classes.textField}
                                margin="normal"
                                variant="outlined"
                            />
                        </CardBody>
                    </Card>
                    <Fab href="#" size="small" color="primary" aria-label="add" className={classes.margin}>
                        <KeyboardBackspaceIcon />
                    </Fab>
                </CardBody>
            </Card>
        </div>

    );
}
