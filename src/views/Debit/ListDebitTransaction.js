import React from "react";
// @material-ui/core components
import { withStyles, makeStyles } from "@material-ui/core/styles";
// core components
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import {
    Row,
    Col
} from 'reactstrap';
import MaterialTable from 'material-table';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import ResetIcon from '@material-ui/icons/Restore';
import SearchIcon from '@material-ui/icons/Search';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import Tooltip from '@material-ui/core/Tooltip';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import MenuItem from '@material-ui/core/MenuItem';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Icon from '@material-ui/core/Icon';
const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "rgba(255,255,255,.62)",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#FFFFFF"
        }
    },
    cardfieldWhite: {
        color: "#FFFFFF",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    tableUpgradeWrapper: {
        display: "block",
        width: "100%",
        overflowX: "auto",
        WebkitOverflowScrolling: "touch",
        MsOverflowStyle: "-ms-autohiding-scrollbar"
    },
    table: {
        width: "100%",
        maxWidth: "100%",
        marginBottom: "1rem",
        backgroundColor: "transparent",
        borderCollapse: "collapse",
        display: "table",
        borderSpacing: "2px",
        borderColor: "grey",
        "& thdead tr th": {
            fontSize: "1.063rem",
            padding: "12px 8px",
            verticalAlign: "middle",
            fontWeight: "300",
            borderTopWidth: "0",
            borderBottom: "1px solid rgba(0, 0, 0, 0.06)",
            textAlign: "inherit"
        },
        "& tbody tr td": {
            padding: "12px 8px",
            verticalAlign: "middle",
            borderTop: "1px solid rgba(0, 0, 0, 0.06)"
        },
        "& td, & th": {
            display: "table-cell"
        }
    },
    center: {
        textAlign: "center"
    },

    input: {
        display: 'none',
    },
};

const useStylesTable = makeStyles({
    root: {
        width: '100%',
        overflowX: 'auto',
    },
    table: {
        minWidth: 700,
    },
});


const currencies = [
    {
        value: '1',
        label: '2019-Retrival request tidak di penuhi dalam batas waktu yang ditentukan',
    },
    {
        value: '2',
        label: '2019-Retrival request tidak di penuhi dalam batas waktu yang ditentukan',
    },
    {
        value: '3',
        label: '2019-Retrival request tidak di penuhi dalam batas waktu yang ditentukan',
    },
    {
        value: '4',
        label: '2019-Retrival request tidak di penuhi dalam batas waktu yang ditentukan',
    },
];
const useStyles = makeStyles(styles);
export default function ListDebitTransaction() {
    const classes = useStyles();
    const classesTable = useStylesTable();
    const [open, setOpen] = React.useState(false);
    const [scroll, setScroll] = React.useState('paper');
    const [currency, setCurrency] = React.useState('EUR');
    const [value, setValue] = React.useState('female');
    const [openRequest, setOpenRequest] = React.useState(false);

    const handleClickOpenRequest = () => {
        setOpenRequest(true);
    };

    const handleCloseRequest = () => {
        setOpenRequest(false);
        handleClose();
    };

    const handleClickOpen = scrollType => () => {
        setOpen(true);
        setScroll(scrollType);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleChange = event => {
        setCurrency(event.target.value);
    };
    const handleChangeRadio = event => {
        setValue(event.target.value);
    };

    const descriptionElementRef = React.useRef(null);
    React.useEffect(() => {
        if (open) {
            const { current: descriptionElement } = descriptionElementRef;
            if (descriptionElement !== null) {
                descriptionElement.focus();
            }
        }
    }, [open]);

    const StyledTableCell = withStyles(theme => ({
        head: {
            backgroundColor: theme.palette.common.black,
            color: theme.palette.common.white,
        },
        body: {
            fontSize: 14,
        },
    }))(TableCell);

    const StyledTableRow = withStyles(theme => ({
        root: {
            '&:nth-of-type(odd)': {
                backgroundColor: theme.palette.common.white,
            },
        },
    }))(TableRow);
    const [state, setState] = React.useState({
        columns: [
            { field: 'claimed', title: 'Claim Status' },
            { field: 'claim_action', title: 'Claim Action' },
            { field: 'card_num', title: 'Nomor Kartu', type: 'numeric' },
            { field: 'ref_num', title: 'Nomor Referensi', type: 'numeric' },
            { field: 'trx_date', title: 'Tanggal Transaksi' },
            { field: 'term_code', title: 'Kode Terminal', type: 'numeric' },
            { field: 'amount', title: 'Nominal transaksi' },
        ],
        //  <Tooltip title="Create Claim Debit" aria-label="add"><Fab onClick={handleClickOpen('paper')} color="primary" size="small" className={classes.fab}><AddIcon /></Fab></Tooltip>
        data: [
            {
                "id": 65,
                "trx_sts": 1,
                "claim_action": <Tooltip title="Create Claim Debit" aria-label="add"><Icon fontSize="small" style={{ color: "green" }} onClick={handleClickOpen('paper')} >add_circle</Icon></Tooltip>,
                "trx_date": "191209",
                "trx_time": "151429",
                "term_code": "10083356        ",
                "card_num": "93600002100833560",
                "mti": "0210",
                "proc_code": "266000",
                "amount": "2006",
                "resp_code": "00",
                "additional_resp_code": null,
                "acquirer": "BRI",
                "issuer": "BCA",
                "ref_num": "000000001211",
                "bit11": "001114",
                "bit32": "93600002",
                "bit38": "A19EPB",
                "bit43": "KOPI KESAN MANIS         KOTA ADM. JAKID",
                "bit49": "360",
                "rcv_date": null,
                "rcv_time": null,
                "business_date": "20191209 00:00",
                "channel_type": 2,
                "destination": null,
                "destination_account": null,
                "destination_owner_name": null,
                "trf_indicator": null,
                "trx_sts_int": null,
                "feeAmount": null,
                "productName": null,
                "nickName": null,
                "biller": null,
                "posRetailCode": null,
                "posApprovalCode": null,
                "posOriginalTransaction": null,
                "posMerchantCategoryCode": "5814",
                "serviceCode": null,
                "debitFlagStatus": null,
                "debitFlagDate": null,
                "switchIssCode": "360002",
                "switchAcqCode": "360004",
                "switchFlagStatus": null,
                "switchFlagDate": null,
                "switchFlaqAcq": null,
                "bit123": null,
                "claimed": false,
                "retrieved": false,
                "claimable": false
            },
            {
                "id": 103,
                "trx_sts": 1,
                "trx_date": "191209",
                "trx_time": "162620",
                "term_code": "310110218       ",
                "card_num": "936000021252732840",
                "mti": "0210",
                "proc_code": "200060",
                "amount": "1065",
                "resp_code": "00",
                "additional_resp_code": null,
                "acquirer": "BNI",
                "issuer": "BRI",
                "ref_num": "100000000376",
                "bit11": "016305",
                "bit32": "93600009",
                "bit38": null,
                "bit43": "QR yap                   Jakarta BaratID",
                "bit49": "360",
                "rcv_date": null,
                "rcv_time": null,
                "business_date": "20191209 00:00",
                "channel_type": 2,
                "destination": null,
                "destination_account": null,
                "destination_owner_name": null,
                "trf_indicator": null,
                "trx_sts_int": null,
                "feeAmount": null,
                "productName": null,
                "nickName": null,
                "biller": null,
                "posRetailCode": null,
                "posApprovalCode": null,
                "posOriginalTransaction": null,
                "posMerchantCategoryCode": "7311",
                "serviceCode": null,
                "debitFlagStatus": null,
                "debitFlagDate": null,
                "switchIssCode": "360004",
                "switchAcqCode": "360001",
                "switchFlagStatus": null,
                "switchFlagDate": null,
                "switchFlaqAcq": null,
                "bit123": "47967556450000000376",
                "claimed": false,
                "retrieved": false,
                "claimable": false
            },
            {
                "id": 91458,
                "trx_sts": 1,
                "trx_date": "191209",
                "trx_time": "135046",
                "term_code": "00075013        ",
                "card_num": "4616994165726755   ",
                "mti": "0210",
                "proc_code": "011000",
                "amount": "1000000",
                "resp_code": "00",
                "additional_resp_code": null,
                "acquirer": "BRI",
                "issuer": "MDR",
                "ref_num": "000000005147",
                "bit11": "009456611962",
                "bit32": "99000000002",
                "bit38": " 42099",
                "bit43": "BANDUNG9844-TSB YGY MAJALENGKA          ",
                "bit49": "360",
                "rcv_date": "0",
                "rcv_time": "134725",
                "business_date": "20191209 00:00",
                "channel_type": 2,
                "destination": null,
                "destination_account": null,
                "destination_owner_name": null,
                "trf_indicator": null,
                "trx_sts_int": null,
                "feeAmount": null,
                "productName": null,
                "nickName": null,
                "biller": null,
                "posRetailCode": null,
                "posApprovalCode": null,
                "posOriginalTransaction": null,
                "posMerchantCategoryCode": null,
                "serviceCode": null,
                "debitFlagStatus": null,
                "debitFlagDate": null,
                "switchIssCode": null,
                "switchAcqCode": null,
                "switchFlagStatus": null,
                "switchFlagDate": null,
                "switchFlaqAcq": null,
                "bit123": null,
                "claimed": false,
                "retrieved": false,
                "claimable": false
            },
            {
                "id": 94290,
                "trx_sts": 1,
                "trx_date": "191209",
                "trx_time": "051204",
                "term_code": "00340572        ",
                "card_num": "4097662866780607   ",
                "mti": "0210",
                "proc_code": "011000",
                "amount": "1000000",
                "resp_code": "00",
                "additional_resp_code": null,
                "acquirer": "BRI",
                "issuer": "MDR",
                "ref_num": "000000000513",
                "bit11": "009456442051",
                "bit32": "99000000002",
                "bit38": "816018",
                "bit43": "MANADO0280-TOKO DIAN                    ",
                "bit49": "360",
                "rcv_date": "0",
                "rcv_time": "050843",
                "business_date": "20191209 00:00",
                "channel_type": 2,
                "destination": null,
                "destination_account": null,
                "destination_owner_name": null,
                "trf_indicator": null,
                "trx_sts_int": null,
                "feeAmount": null,
                "productName": null,
                "nickName": null,
                "biller": null,
                "posRetailCode": null,
                "posApprovalCode": null,
                "posOriginalTransaction": null,
                "posMerchantCategoryCode": null,
                "serviceCode": null,
                "debitFlagStatus": null,
                "debitFlagDate": null,
                "switchIssCode": null,
                "switchAcqCode": null,
                "switchFlagStatus": null,
                "switchFlagDate": null,
                "switchFlaqAcq": null,
                "bit123": null,
                "claimed": false,
                "retrieved": false,
                "claimable": false
            },
            {
                "id": 334,
                "trx_sts": 1,
                "trx_date": "191209",
                "trx_time": "030232",
                "term_code": "0002081521553481",
                "card_num": "9360000900015041023",
                "mti": "0210",
                "proc_code": "200010",
                "amount": "2017",
                "resp_code": "00",
                "additional_resp_code": null,
                "acquirer": "TMO",
                "issuer": "BNI",
                "ref_num": "102531769981",
                "bit11": "017564",
                "bit32": "93600898",
                "bit38": "548833",
                "bit43": "Bakso JonoJakarta 123ID",
                "bit49": "360",
                "rcv_date": null,
                "rcv_time": null,
                "business_date": "20191209 00:00",
                "channel_type": 2,
                "destination": null,
                "destination_account": null,
                "destination_owner_name": null,
                "trf_indicator": null,
                "trx_sts_int": null,
                "feeAmount": null,
                "productName": null,
                "nickName": null,
                "biller": null,
                "posRetailCode": null,
                "posApprovalCode": null,
                "posOriginalTransaction": null,
                "posMerchantCategoryCode": "5499",
                "serviceCode": null,
                "debitFlagStatus": null,
                "debitFlagDate": null,
                "switchIssCode": "360001",
                "switchAcqCode": "360004",
                "switchFlagStatus": null,
                "switchFlagDate": null,
                "switchFlaqAcq": null,
                "bit123": "68074182110741821110",
                "claimed": false,
                "retrieved": false,
                "claimable": false
            },
            {
                "id": 335,
                "trx_sts": 1,
                "trx_date": "191209",
                "trx_time": "030200",
                "term_code": "0002081521553481",
                "card_num": "9360000900015041023",
                "mti": "0210",
                "proc_code": "200010",
                "amount": "1516",
                "resp_code": "00",
                "additional_resp_code": null,
                "acquirer": "TMO",
                "issuer": "BNI",
                "ref_num": "091021250881",
                "bit11": "017563",
                "bit32": "93600898",
                "bit38": "793137",
                "bit43": "Bakso JonoJakarta 123ID",
                "bit49": "360",
                "rcv_date": null,
                "rcv_time": null,
                "business_date": "20191209 00:00",
                "channel_type": 2,
                "destination": null,
                "destination_account": null,
                "destination_owner_name": null,
                "trf_indicator": null,
                "trx_sts_int": null,
                "feeAmount": null,
                "productName": null,
                "nickName": null,
                "biller": null,
                "posRetailCode": null,
                "posApprovalCode": null,
                "posOriginalTransaction": null,
                "posMerchantCategoryCode": "5499",
                "serviceCode": null,
                "debitFlagStatus": null,
                "debitFlagDate": null,
                "switchIssCode": "360001",
                "switchAcqCode": "360004",
                "switchFlagStatus": null,
                "switchFlagDate": null,
                "switchFlaqAcq": null,
                "bit123": "74209782112097821110",
                "claimed": false,
                "retrieved": false,
                "claimable": false
            },
            {
                "id": 1126,
                "trx_sts": 1,
                "trx_date": "191205",
                "trx_time": "144644",
                "term_code": "0000000069299027",
                "card_num": "9360000811234567890",
                "mti": "0210",
                "proc_code": "266000",
                "amount": "100000",
                "resp_code": "00",
                "additional_resp_code": null,
                "acquirer": "MDR",
                "issuer": "BNI",
                "ref_num": "113779833814",
                "bit11": "005444",
                "bit32": "93600008",
                "bit38": null,
                "bit43": "Waroeng Digital          JAKARTA SELATID",
                "bit49": "360",
                "rcv_date": null,
                "rcv_time": null,
                "business_date": "20191209 00:00",
                "channel_type": 2,
                "destination": null,
                "destination_account": null,
                "destination_owner_name": null,
                "trf_indicator": null,
                "trx_sts_int": null,
                "feeAmount": null,
                "productName": null,
                "nickName": null,
                "biller": null,
                "posRetailCode": null,
                "posApprovalCode": null,
                "posOriginalTransaction": null,
                "posMerchantCategoryCode": "5499",
                "serviceCode": null,
                "debitFlagStatus": null,
                "debitFlagDate": null,
                "switchIssCode": "360001",
                "switchAcqCode": "360004",
                "switchFlagStatus": null,
                "switchFlagDate": null,
                "switchFlaqAcq": null,
                "bit123": null,
                "claimed": false,
                "retrieved": false,
                "claimable": false
            }
        ]
    });
    return (
        <div>

            <Paper className={classesTable.root}>
                <Table className={classesTable.table} aria-label="customized table">
                    <TableBody>
                        <StyledTableRow >
                            <StyledTableCell component="th" scope="row">
                                Nomor Kartu
                                    </StyledTableCell>
                            <StyledTableCell align="left">
                                <div className="row-transaction">
                                    <TextField
                                        id="outlined-textarea"
                                        className={classes.TextField}
                                        variant="outlined"
                                        fullWidth
                                        margin="dense"
                                    />
                                </div>
                            </StyledTableCell>
                        </StyledTableRow>
                        <StyledTableRow >
                            <StyledTableCell component="th" scope="row">
                                Tanggal Transaksi
                                    </StyledTableCell>
                            <StyledTableCell align="left">
                                <div className="row-transaction">
                                    <TextField
                                        className={classes.TextField}
                                        variant="outlined"
                                        fullWidth
                                        margin="dense"
                                        id="date"
                                        type="date"
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                    />
                                </div>
                            </StyledTableCell>
                        </StyledTableRow>
                        <StyledTableRow >
                            <StyledTableCell component="th" scope="row">
                                Nomor Referensi
                                    </StyledTableCell>
                            <StyledTableCell align="left">
                                <div className="row-transaction">
                                    <TextField
                                        id="outlined-textarea"
                                        className={classes.TextField}
                                        variant="outlined"
                                        fullWidth
                                        margin="dense"
                                    />
                                </div>
                            </StyledTableCell>
                        </StyledTableRow>
                        <StyledTableRow >
                            <StyledTableCell component="th" scope="row">
                                Terminal Code
                                    </StyledTableCell>
                            <StyledTableCell align="left">
                                <div className="row-transaction">
                                    <TextField
                                        id="outlined-textarea"
                                        className={classes.TextField}
                                        variant="outlined"
                                        fullWidth
                                        margin="dense"
                                    />
                                </div>
                            </StyledTableCell>
                        </StyledTableRow>
                        <StyledTableRow >
                            <StyledTableCell component="th" scope="row">
                                Lokasi Merchant
                                    </StyledTableCell>
                            <StyledTableCell align="left">
                                <div className="row-transaction">
                                    <TextField
                                        id="outlined-textarea"
                                        variant="outlined"
                                        fullWidth
                                        margin="dense"
                                    />
                                </div>
                            </StyledTableCell>
                        </StyledTableRow>
                        <StyledTableRow >
                            <StyledTableCell component="th" scope="row">
                                Nominal
                                    </StyledTableCell>
                            <StyledTableCell align="left">
                                <div className="row-transaction">
                                    <TextField
                                        id="outlined-textarea"
                                        className={classes.TextField}
                                        variant="outlined"
                                        fullWidth
                                        margin="dense"
                                    />
                                </div>
                            </StyledTableCell>
                        </StyledTableRow>
                    </TableBody>
                </Table>
            </Paper>
            <div className="row-transaction-btn">
                <Button style={{ marginRight: '1rem' }}
                    variant="contained"
                    className={classes.button}
                >
                    Reset  <ResetIcon />
                </Button>
                {/* This Button uses a Font Icon, see the installation instructions in the Icon component docs. */}
                <Button
                    variant="contained"
                    color="primary"
                    className={classes.button}
                >
                    Search <SearchIcon />
                </Button>
            </div>
            <MaterialTable
                title="List Of QRDebit Transactions"
                options={{
                    search: false
                }}
                field="Editable Example"
                columns={state.columns}
                data={state.data}

            />
            <Dialog
                open={open}
                onClose={handleClose}
                scroll={scroll}
                aria-labelledby="scroll-dialog-title"
                aria-describedby="scroll-dialog-description"
            >
                <DialogTitle id="scroll-dialog-title">Create QR Claim Online</DialogTitle>
                <DialogContent dividers={scroll === 'paper'}>
                    <DialogTitle id="scroll-dialog-title">Transaction Information</DialogTitle>
                    <TextField
                        fullWidth
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Tanggal Transaksi"
                        defaultValue="2019-01-01"
                        className={classes.textField}
                        margin="dense"
                    />
                    <TextField
                        fullWidth
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Waktu Transaksi"
                        defaultValue="21:00:09"
                        className={classes.textField}
                        margin="dense"
                    />
                    <TextField
                        fullWidth
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Nomor Kartu"
                        defaultValue="201909438509458"
                        className={classes.textField}
                        margin="dense"
                    />
                    <TextField
                        fullWidth
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Nomor Transaksi"
                        defaultValue="200,900.00"
                        className={classes.textField}
                        margin="dense"
                    />
                    <TextField
                        fullWidth
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Status Transaksi"
                        defaultValue="Berhasil"
                        className={classes.textField}
                        margin="dense"
                    />
                    <TextField
                        fullWidth
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Kode Terminal"
                        defaultValue="103924789"
                        className={classes.textField}
                        margin="dense"
                    />
                    <TextField
                        fullWidth
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Kode Pemrosesan"
                        defaultValue="0000000000"
                        className={classes.textField}
                        margin="dense"
                    />
                    <TextField
                        fullWidth
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Kode Respons"
                        defaultValue="00"
                        className={classes.textField}
                        margin="dense"
                    />
                    <TextField
                        fullWidth
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Issuer"
                        defaultValue="MDR"
                        className={classes.textField}
                        margin="dense"
                    />
                    <TextField
                        fullWidth
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Acquirer"
                        defaultValue="BRI"
                        className={classes.textField}
                        margin="dense"
                    />
                    <TextField
                        fullWidth
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Nama Merchant"
                        defaultValue="Mitra Adira Utama"
                        className={classes.textField}
                        margin="dense"
                    />
                    <TextField
                        fullWidth
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Lokasi Merchant"
                        defaultValue="Pergudangan CID 87"
                        className={classes.textField}
                        margin="dense"
                    />
                    <TextField
                        fullWidth
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Nomor Referensi"
                        defaultValue="09897876867868"
                        className={classes.textField}
                        margin="dense"
                    />
                    <TextField
                        fullWidth
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Status Klaim"
                        defaultValue="Claimable"
                        className={classes.textField}
                        margin="dense"
                    />
                    <DialogTitle id="scroll-dialog-title">Input Claim</DialogTitle>
                    <TextField
                        label="Tanggal Claim"
                        placeholder="Tanggal Claim"
                        className={classes.TextField}
                        variant="outlined"
                        fullWidth
                        margin="dense"
                        id="date"
                        type="date"
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
                    <TextField
                        id="outlined-select-currency"
                        select
                        fullWidth
                        label="Reason Code"
                        className={classes.textField}
                        value={currency}
                        onChange={handleChange}
                        SelectProps={{
                            MenuProps: {
                                className: classes.menu,
                            },
                        }}
                        margin="dense"
                        variant="outlined"
                    >
                        {currencies.map(option => (
                            <MenuItem key={option.value} value={option.value}>
                                {option.label}
                            </MenuItem>
                        ))}
                    </TextField>
                    <TextField
                        label="No Registrasi Claim"
                        placeholder="No Registrasi Claim"
                        className={classes.TextField}
                        variant="outlined"
                        fullWidth
                        margin="dense"
                    />
                    <TextField
                        disabled
                        label="Jenis Claim"
                        placeholder="Jenis Claim"
                        className={classes.TextField}
                        variant="outlined"
                        defaultValue="Chargeback"
                        fullWidth
                        margin="dense"
                    />

                    <TextField
                        fullWidth
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Status Claim"
                        defaultValue="New"
                        className={classes.textField}
                        margin="dense"
                    />
                    <FormControl component="fieldset" className={classes.formControl}>
                        <FormLabel >Jenis Nominal Claim</FormLabel>
                        <RadioGroup aria-label="gender" name="gender1" value={value} onChange={handleChangeRadio}>
                            <FormControlLabel value="fullamount" control={<Radio />} label="Full Amount" />
                            <FormControlLabel value="partialamount" control={<Radio />} label="Partial Amount" />
                        </RadioGroup>
                    </FormControl>
                    <TextField
                        disabled
                        label="Nominal Claim"
                        placeholder="Nominal Claim"
                        className={classes.TextField}
                        variant="outlined"
                        fullWidth
                        defaultValue="283,764.00"
                        margin="dense"
                    />
                    <TextField
                        id="outlined-multiline-static"
                        label="Deskripsi"
                        multiline
                        fullWidth
                        rows="4"
                        className={classes.textField}
                        margin="normal"
                        variant="outlined"
                    />
                    <TextField
                        fullWidth
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Pembuat Claim"
                        defaultValue="MDR"
                        className={classes.textField}
                        margin="dense"
                    />
                    <TextField
                        fullWidth
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Penerima Claim"
                        defaultValue="BRI"
                        className={classes.textField}
                        margin="dense"
                    />
                    <input
                        accept="image/*"
                        className={classes.input}
                        id="outlined-button-file"
                        multiple
                        type="file"
                    />
                    <Row>
                        <Col>
                            <FormLabel >Evidence</FormLabel>
                        </Col>
                        <Col>
                            <Row>
                                <Col>
                                    <label htmlFor="outlined-button-file">
                                        <Button variant="outlined" component="span">
                                            Upload
                                        </Button>
                                    </label>
                                </Col>
                                <Col>
                                    <TextField
                                        fullWidth
                                        disabled
                                        variant="outlined"
                                        id="standard-disabled"
                                        defaultValue="FCB159328.doc"
                                        className={classes.textField}
                                        margin="dense"
                                    />
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <label htmlFor="outlined-button-file">
                                        <Button variant="outlined" component="span">
                                            Upload
                                        </Button>
                                    </label>
                                </Col>
                                <Col>
                                    <TextField
                                        fullWidth
                                        disabled
                                        variant="outlined"
                                        id="standard-disabled"
                                        defaultValue="FCB129328.doc"
                                        className={classes.textField}
                                        margin="dense"
                                    />
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <label htmlFor="outlined-button-file">
                                        <Button variant="outlined" component="span">
                                            Upload
                                        </Button>
                                    </label>
                                </Col>
                                <Col>
                                    <TextField
                                        fullWidth
                                        disabled
                                        variant="outlined"
                                        id="standard-disabled"
                                        className={classes.textField}
                                        margin="dense"
                                    />
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClickOpenRequest} variant="contained" size="small" color="primary" className={classes.margin}>
                        save
                    </Button>
                    <Button href="/admin/viewdebitclaim" variant="contained" size="small" color="primary" className={classes.margin}>
                        view & edit
                    </Button>
                    <Button onClick={handleClickOpenRequest} variant="contained" size="small" color="primary" className={classes.margin}>
                        submit for approval
                    </Button>
                    <Button onClick={handleClose} variant="contained" size="small" color="primary" className={classes.margin}>
                        cancel
                    </Button>
                </DialogActions>
            </Dialog>
            <Dialog
                open={openRequest}
                onClose={handleCloseRequest}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">{"Request Submited"}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        <FormLabel >Save First Chargeback</FormLabel>
                        <TextField
                            fullWidth
                            disabled
                            variant="outlined"
                            id="standard-disabled"
                            defaultValue="FCB129328.doc"
                            className={classes.textField}
                            margin="dense"
                        />
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseRequest} color="primary" autoFocus>
                        Continue
                    </Button>
                </DialogActions>
            </Dialog>
        </div>

    );
}
