import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import MaterialTable from 'material-table';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import ResetIcon from '@material-ui/icons/Restore';
import SearchIcon from '@material-ui/icons/Search';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import Tooltip from '@material-ui/core/Tooltip';
const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "rgba(255,255,255,.62)",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#FFFFFF"
        }
    },
    cardfieldWhite: {
        color: "#FFFFFF",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    tableUpgradeWrapper: {
        display: "block",
        width: "100%",
        overflowX: "auto",
        WebkitOverflowScrolling: "touch",
        MsOverflowStyle: "-ms-autohiding-scrollbar"
    },
    table: {
        width: "100%",
        maxWidth: "100%",
        marginBottom: "1rem",
        backgroundColor: "transparent",
        borderCollapse: "collapse",
        display: "table",
        borderSpacing: "2px",
        borderColor: "grey",
        "& thdead tr th": {
            fontSize: "1.063rem",
            padding: "12px 8px",
            verticalAlign: "middle",
            fontWeight: "300",
            borderTopWidth: "0",
            borderBottom: "1px solid rgba(0, 0, 0, 0.06)",
            textAlign: "inherit"
        },
        "& tbody tr td": {
            padding: "12px 8px",
            verticalAlign: "middle",
            borderTop: "1px solid rgba(0, 0, 0, 0.06)"
        },
        "& td, & th": {
            display: "table-cell"
        }
    },
    center: {
        textAlign: "center"
    }
};
const useStyles = makeStyles(styles);
export default function CreateClaimDebit() {
    const classes = useStyles();

    const [state, setState] = React.useState({
        columns: [
            { field: 'claim_status', title: 'Claim Status' },
            { field: 'claim_action', title: 'Claim Action' },
            { field: 'nomor_kartu', title: 'Nomor Kartu', type: 'numeric' },
            { field: 'nomor_referensi', title: 'Nomor Referensi', type: 'numeric' },
            { field: 'Tanggal_transaksi', title: 'Tanggal Transaksi' },
            { field: 'kode_terminal', title: 'Kode Terminal', type: 'numeric' },
            { field: 'nominal_transaksi', title: 'Nominal transaksi' },
        ],
        data: [
            {
                claim_status: 'unclaim',
                claim_action: <Tooltip title="Create Claim Debit" aria-label="add"><Fab color="primary" size="small" className={classes.fab}><AddIcon /></Fab></Tooltip>,
                nomor_kartu: 99198700,
                nomor_referensi: 63,
                Tanggal_transaksi: '2009/07/07',
                kode_terminal: 10923989,
                nominal_transaksi: '290,890.00'
            },
            {
                claim_status: 'unclaim',
                claim_action: <Tooltip title="Create Claim Debit" aria-label="add"><Fab color="primary" size="small" className={classes.fab}><AddIcon /></Fab></Tooltip>,
                nomor_kartu: 99198700,
                nomor_referensi: 63,
                Tanggal_transaksi: '2009/07/07',
                kode_terminal: 10923989,
                nominal_transaksi: '290,890.00'
            },
            {
                claim_status: 'unclaim',
                claim_action: <Tooltip title="Create Claim Debit" aria-label="add"><Fab color="primary" size="small" className={classes.fab}><AddIcon /></Fab></Tooltip>,
                nomor_kartu: 99198700,
                nomor_referensi: 63,
                Tanggal_transaksi: '2009/07/07',
                kode_terminal: 10923989,
                nominal_transaksi: '290,890.00'
            },
            {
                claim_status: 'unclaim',
                claim_action: <Tooltip title="Create Claim Debit" aria-label="add"><Fab color="primary" size="small" className={classes.fab}><AddIcon /></Fab></Tooltip>,
                nomor_kartu: 99198700,
                nomor_referensi: 63,
                Tanggal_transaksi: '2009/07/07',
                kode_terminal: 10923989,
                nominal_transaksi: '290,890.00'
            },
            {
                claim_status: 'unclaim',
                claim_action: <Tooltip title="Create Claim Debit" aria-label="add"><Fab color="primary" size="small" className={classes.fab}><AddIcon /></Fab></Tooltip>,
                nomor_kartu: 99198700,
                nomor_referensi: 63,
                Tanggal_transaksi: '2009/07/07',
                kode_terminal: 10923989,
                nominal_transaksi: '290,890.00'
            },
            {
                claim_status: 'unclaim',
                claim_action: <Tooltip title="Create Claim Debit" aria-label="add"><Fab color="primary" size="small" className={classes.fab}><AddIcon /></Fab></Tooltip>,
                nomor_kartu: 99198700,
                nomor_referensi: 63,
                Tanggal_transaksi: '2009/07/07',
                kode_terminal: 10923989,
                nominal_transaksi: '290,890.00'
            },
        ],
    });
    return (
        <div>
            <Card>
                <CardBody>
                    <div className="row-transaction">
                        <TextField
                            id="outlined-textarea"
                            label="Nomor Kartu"
                            placeholder="Nomor Kartu"
                            className={classes.TextField}
                            margin="normal"
                            variant="outlined"
                            fullWidth
                            margin="dense"
                        />
                    </div>
                    <div className="row-transaction">
                        <TextField
                            label="Tanggal Transaksi"
                            placeholder="Tanggal Transaksi"
                            className={classes.TextField}
                            margin="normal"
                            variant="outlined"
                            fullWidth
                            margin="dense"
                            id="date"
                            type="date"
                            className={classes.textField}
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                    </div>
                    <div className="row-transaction">
                        <TextField
                            id="outlined-textarea"
                            label="Nomor Referensi"
                            placeholder="Nomor Referensi"
                            className={classes.TextField}
                            margin="normal"
                            variant="outlined"
                            fullWidth
                            margin="dense"
                        />
                    </div>
                    <div className="row-transaction">
                        <TextField
                            id="outlined-textarea"
                            label="Terminal Code"
                            placeholder="Terminal Code"
                            className={classes.TextField}
                            margin="normal"
                            variant="outlined"
                            fullWidth
                            margin="dense"
                        />
                    </div>
                    <div className="row-transaction">
                        <TextField
                            id="outlined-textarea"
                            label="Lokasi Merchant"
                            placeholder="Lokasi Merchant"
                            className={classes.TextField}
                            margin="normal"
                            variant="outlined"
                            fullWidth
                            margin="dense"
                        />
                    </div>
                    <div className="row-transaction">
                        <TextField
                            id="outlined-textarea"
                            label="Nominal"
                            placeholder="Nominal"
                            className={classes.TextField}
                            margin="normal"
                            variant="outlined"
                            fullWidth
                            margin="dense"
                        />
                    </div>

                    <div className="row-transaction-btn">
                        <Button style={{ marginRight: '1rem' }}
                            variant="contained"
                            color="secondary"
                            className={classes.button}
                        >
                            Reset  <ResetIcon />
                        </Button>
                        {/* This Button uses a Font Icon, see the installation instructions in the Icon component docs. */}
                        <Button
                            variant="contained"
                            color="primary"
                            className={classes.button}
                        >
                            Search <SearchIcon />
                        </Button>
                    </div>

                </CardBody>
            </Card>
            <MaterialTable
                title="List Of QRDebit Transactions"
                options={{
                    search: false
                }}
                field="Editable Example"
                columns={state.columns}
                data={state.data}

            />
        </div>

    );
}
