import React, { useState, useEffect } from "react";
// @material-ui/core components
import { withStyles, makeStyles } from "@material-ui/core/styles";
import { Col, Row } from 'reactstrap';
// core components
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import MaterialTable from 'material-table';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import ResetIcon from '@material-ui/icons/Restore';
import SearchIcon from '@material-ui/icons/Search';
import Fab from '@material-ui/core/Fab';
import RemoveIcon from '@material-ui/icons/Remove';
import Tooltip from '@material-ui/core/Tooltip';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import MenuItem from '@material-ui/core/MenuItem';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Icon from '@material-ui/core/Icon';
const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "rgba(255,255,255,.62)",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#FFFFFF"
        }
    },
    cardfieldWhite: {
        color: "#FFFFFF",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    tableUpgradeWrapper: {
        display: "block",
        width: "100%",
        overflowX: "auto",
        WebkitOverflowScrolling: "touch",
        MsOverflowStyle: "-ms-autohiding-scrollbar"
    },
    table: {
        width: "100%",
        maxWidth: "100%",
        marginBottom: "1rem",
        backgroundColor: "transparent",
        borderCollapse: "collapse",
        display: "table",
        borderSpacing: "2px",
        borderColor: "grey",
        "& thdead tr th": {
            fontSize: "1.063rem",
            padding: "12px 8px",
            verticalAlign: "middle",
            fontWeight: "300",
            borderTopWidth: "0",
            borderBottom: "1px solid rgba(0, 0, 0, 0.06)",
            textAlign: "inherit"
        },
        "& tbody tr td": {
            padding: "12px 8px",
            verticalAlign: "middle",
            borderTop: "1px solid rgba(0, 0, 0, 0.06)"
        },
        "& td, & th": {
            display: "table-cell"
        }
    },
    center: {
        textAlign: "center"
    }
};

const useStylesTable = makeStyles({
    root: {
        width: '100%',
        overflowX: 'auto',
    },
    table: {
        minWidth: 700,
    },
});


const currencies = [
    {
        value: '1',
        label: 'All',
    },
    {
        value: '2',
        label: 'pilihan 2',
    },
    {
        value: '3',
        label: 'pilihan 3',
    },
    {
        value: '4',
        label: 'pilihan 4',
    },
];

const useStyles = makeStyles(styles);
export default function ListDebitClaim() {
    const classes = useStyles();
    const classesTable = useStylesTable();
    const [open, setOpen] = React.useState(false);
    const [scroll, setScroll] = React.useState('paper');
    const [currency, setCurrency] = React.useState('EUR');
    const [value, setValue] = React.useState('female');
    const [todos, setTodos] = useState([]);
    const [openRequest, setOpenRequest] = React.useState(false);

    const handleClickOpenRequest = () => {
        setOpenRequest(true);
    };
    const handleCloseRequest = () => {
        setOpenRequest(false);
        handleClose();
    };

    const handleClickOpen = scrollType => () => {
        setOpen(true);
        setScroll(scrollType);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleChange = event => {
        setCurrency(event.target.value);
    };
    const handleChangeRadio = event => {
        setValue(event.target.value);
    };
    const descriptionElementRef = React.useRef(null);
    useEffect(() => {
        if (open) {
            const { current: descriptionElement } = descriptionElementRef;
            if (descriptionElement !== null) {
                descriptionElement.focus();
            }
        }
    }, [open]);
    useEffect(() => {

        fetch('http://192.168.180.54:8009/issuers')
            .then(res => res.json())
            .then(items => setTodos(items.data.item))
            .catch(error => console.log('parsing failed', error))

    }, []);

    const StyledTableCell = withStyles(theme => ({
        head: {
            backgroundColor: theme.palette.common.black,
            color: theme.palette.common.white,
        },
        body: {
            fontSize: 14,
        },
    }))(TableCell);

    const StyledTableRow = withStyles(theme => ({
        root: {
            '&:nth-of-type(odd)': {
                backgroundColor: theme.palette.common.white,
            },
        },
    }))(TableRow);
    const btn =
        <Row>
            <Col style={{ width: '40%', float: 'left' }}>
                <Tooltip title="info" aria-label="add">
                    <Icon fontSize="small" style={{ color: "blue" }} onClick={handleClickOpen('paper')} >info</Icon>
                </Tooltip>
            </Col>
            <Col style={{ width: '40%', float: 'right' }}>
                <Tooltip title="Reversal" aria-label="add">
                    <Icon fontSize="small" style={{ color: "green" }} onClick={handleClickOpen('paper')} >remove_circle</Icon>
                </Tooltip>
            </Col>
        </Row>;
    const [state, setState] = React.useState({
        columns: [
            { field: 'action', title: 'Action' },
            { field: 'no_registrasi_claim', title: 'Nomor Registrasi Claim' },
            { field: 'status_claim', title: 'Status Claim' },
            { field: 'tgl_status_claim', title: 'Tanggal Status Claim' },
            { field: 'nomor_kartu', title: 'Nomor Kartu' },
            { field: 'nomor_referensi', title: 'Nomor Referensi' },
            { field: 'tgl_transaksi', title: 'Tanggal Transaksi' },
            { field: 'kode_terminal', title: 'Kode Terminal' },
        ],
        data: [
            {
                action: btn,
                no_registrasi_claim: "99198700",
                status_claim: "Pending member Approval",
                tgl_status_claim: '2009-07-07',
                nomor_kartu: "109239894567",
                nomor_referensi: '290,890.00',
                tgl_transaksi: "2019-09-08",
                kode_terminal: "098987898798",
            },
            {
                action: btn,
                no_registrasi_claim: "99198700",
                status_claim: "Pending member Approval",
                tgl_status_claim: '2009-07-07',
                nomor_kartu: "109239894567",
                nomor_referensi: '290,890.00',
                tgl_transaksi: "2019-09-08",
                kode_terminal: "098987898798",
            }, {
                action: btn,
                no_registrasi_claim: "99198700",
                status_claim: "Cleared",
                tgl_status_claim: '2009-07-07',
                nomor_kartu: "109239894567",
                nomor_referensi: '290,890.00',
                tgl_transaksi: "2019-09-08",
                kode_terminal: "098987898798",
            }, {
                action: btn,
                no_registrasi_claim: "99198700",
                status_claim: "Expired",
                tgl_status_claim: '2009-07-07',
                nomor_kartu: "109239894567",
                nomor_referensi: '290,890.00',
                tgl_transaksi: "2019-09-08",
                kode_terminal: "098987898798",
            }, {
                action: btn,
                no_registrasi_claim: "99198700",
                status_claim: "Pending member Approval",
                tgl_status_claim: '2009-07-07',
                nomor_kartu: "109239894567",
                nomor_referensi: '290,890.00',
                tgl_transaksi: "2019-09-08",
                kode_terminal: "098987898798",
            }, {
                action: btn,
                no_registrasi_claim: "99198700",
                status_claim: "Pending member Approval",
                tgl_status_claim: '2009-07-07',
                nomor_kartu: "109239894567",
                nomor_referensi: '290,890.00',
                tgl_transaksi: "2019-09-08",
                kode_terminal: "098987898798",
            }, {
                action: btn,
                no_registrasi_claim: "99198700",
                status_claim: "Pending member Approval",
                tgl_status_claim: '2009-07-07',
                nomor_kartu: "109239894567",
                nomor_referensi: '290,890.00',
                tgl_transaksi: "2019-09-08",
                kode_terminal: "098987898798",
            },
        ],
    });
    return (
        <div>

            <Paper className={classesTable.root}>
                <Table className={classesTable.table} aria-label="customized table">
                    <TableBody>
                        <StyledTableRow >
                            <StyledTableCell component="th" scope="row">
                                Nomor Kartu
                                    </StyledTableCell>
                            <StyledTableCell align="left">
                                <div className="row-transaction-claim">
                                    <TextField
                                        id="outlined-textarea"
                                        className={classes.TextField}
                                        variant="outlined"
                                        fullWidth
                                        margin="dense"
                                    />
                                </div>
                            </StyledTableCell>
                        </StyledTableRow>
                        <StyledTableRow >
                            <StyledTableCell component="th" scope="row">
                                Tanggal Transaksi
                                    </StyledTableCell>
                            <StyledTableCell align="left">
                                <div className="row-transaction-claim">
                                    <TextField
                                        className={classes.TextField}
                                        variant="outlined"
                                        fullWidth
                                        margin="dense"
                                        id="date"
                                        type="date"
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                    />
                                </div>
                            </StyledTableCell>
                        </StyledTableRow>
                        <StyledTableRow >
                            <StyledTableCell component="th" scope="row">
                                Status Claim
                                    </StyledTableCell>
                            <StyledTableCell align="left">
                                <div className="row-transaction-claim">
                                    <TextField
                                        id="outlined-select-currency"
                                        select
                                        fullWidth
                                        className={classes.textField}
                                        value={currency}
                                        onChange={handleChange}
                                        SelectProps={{
                                            MenuProps: {
                                                className: classes.menu,
                                            },
                                        }}
                                        margin="dense"
                                        variant="outlined"
                                    >
                                        {currencies.map(option => (
                                            <MenuItem key={option.value} value={option.value}>
                                                {option.label}
                                            </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                            </StyledTableCell>
                        </StyledTableRow>
                        <StyledTableRow >
                            <StyledTableCell component="th" scope="row">
                                Role Bank
                                    </StyledTableCell>
                            <StyledTableCell align="left">
                                <div className="row-transaction-claim">
                                    <TextField
                                        id="outlined-select-currency"
                                        select
                                        fullWidth
                                        className={classes.textField}
                                        value={currency}
                                        onChange={handleChange}
                                        SelectProps={{
                                            MenuProps: {
                                                className: classes.menu,
                                            },
                                        }}
                                        margin="dense"
                                        variant="outlined"
                                    >
                                        {currencies.map(option => (
                                            <MenuItem key={option.value} value={option.value}>
                                                {option.label}
                                            </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                            </StyledTableCell>
                        </StyledTableRow>
                        <StyledTableRow >
                            <StyledTableCell component="th" scope="row">
                                Jenis Claim
                                    </StyledTableCell>
                            <StyledTableCell align="left">
                                <div className="row-transaction-claim">
                                    <TextField
                                        id="outlined-select-currency"
                                        select
                                        fullWidth
                                        className={classes.textField}
                                        value={currency}
                                        onChange={handleChange}
                                        SelectProps={{
                                            MenuProps: {
                                                className: classes.menu,
                                            },
                                        }}
                                        margin="dense"
                                        variant="outlined"
                                    >
                                        {currencies.map(option => (
                                            <MenuItem key={option.value} value={option.value}>
                                                {option.label}
                                            </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                            </StyledTableCell>
                        </StyledTableRow>
                        <StyledTableRow >
                            <StyledTableCell component="th" scope="row">
                                Tanggal Claim
                            </StyledTableCell>
                            <StyledTableCell align="left">
                                <div className="row-transaction-claim">
                                    <TextField
                                        className={classes.TextField}
                                        variant="outlined"
                                        fullWidth
                                        margin="dense"
                                        id="date"
                                        type="date"
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                    />
                                </div>
                            </StyledTableCell>
                        </StyledTableRow>
                        <StyledTableRow >
                            <StyledTableCell component="th" scope="row">
                                No Registrasi Claim
                            </StyledTableCell>
                            <StyledTableCell align="left">
                                <div className="row-transaction-claim">
                                    <TextField
                                        id="outlined-textarea"
                                        className={classes.TextField}
                                        variant="outlined"
                                        fullWidth
                                        margin="dense"
                                    />
                                </div>
                            </StyledTableCell>
                        </StyledTableRow>
                        <StyledTableRow >
                            <StyledTableCell component="th" scope="row">
                                Pembuat Claim
                                    </StyledTableCell>
                            <StyledTableCell align="left">
                                <div className="row-transaction-claim">
                                    <TextField
                                        id="outlined-select-currency"
                                        select
                                        fullWidth
                                        className={classes.textField}
                                        value={currency}
                                        onChange={handleChange}
                                        SelectProps={{
                                            MenuProps: {
                                                className: classes.menu,
                                            },
                                        }}
                                        margin="dense"
                                        variant="outlined"
                                    >
                                        {currencies.map(option => (
                                            <MenuItem key={option.value} value={option.value}>
                                                {option.label}
                                            </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                            </StyledTableCell>
                        </StyledTableRow>
                        <StyledTableRow >
                            <StyledTableCell component="th" scope="row">
                                Penerima Claim
                                    </StyledTableCell>
                            <StyledTableCell align="left">
                                <div className="row-transaction-claim">
                                    <TextField
                                        id="outlined-select-currency"
                                        select
                                        fullWidth
                                        className={classes.textField}
                                        value={currency}
                                        onChange={handleChange}
                                        SelectProps={{
                                            MenuProps: {
                                                className: classes.menu,
                                            },
                                        }}
                                        margin="dense"
                                        variant="outlined"
                                    >
                                        {currencies.map(option => (
                                            <MenuItem key={option.value} value={option.value}>
                                                {option.label}
                                            </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                            </StyledTableCell>
                        </StyledTableRow>
                        <StyledTableRow >
                            <StyledTableCell component="th" scope="row">
                                Created By
                            </StyledTableCell>
                            <StyledTableCell align="left">
                                <div className="row-transaction-claim">
                                    <TextField
                                        id="outlined-select-currency"
                                        select
                                        fullWidth
                                        className={classes.textField}
                                        value={currency}
                                        onChange={handleChange}
                                        SelectProps={{
                                            MenuProps: {
                                                className: classes.menu,
                                            },
                                        }}
                                        margin="dense"
                                        variant="outlined"
                                    >
                                        {currencies.map(option => (
                                            <MenuItem key={option.value} value={option.value}>
                                                {option.label}
                                            </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                            </StyledTableCell>
                        </StyledTableRow>
                        <StyledTableRow >
                            <StyledTableCell component="th" scope="row">
                                Nominal
                            </StyledTableCell>
                            <StyledTableCell align="left">
                                <div className="row-transaction-claim">
                                    <TextField
                                        id="outlined-textarea"
                                        className={classes.TextField}
                                        variant="outlined"
                                        fullWidth
                                        margin="dense"
                                    />
                                </div>
                            </StyledTableCell>
                        </StyledTableRow>

                    </TableBody>
                </Table>
            </Paper>
            <div className="row-transaction-claim-btn">
                <Button style={{ marginRight: '1rem' }}
                    variant="contained"
                    className={classes.button}
                >
                    Reset  <ResetIcon />
                </Button>
                {/* This Button uses a Font Icon, see the installation instructions in the Icon component docs. */}
                <Button
                    variant="contained"
                    color="primary"
                    className={classes.button}
                >
                    Search <SearchIcon />
                </Button>
            </div>
            <MaterialTable
                title="List of Created Debit Claim"
                options={{
                    search: false
                }}
                field="Editable Example"
                columns={state.columns}
                data={state.data}

            />
            <Dialog
                open={open}
                onClose={handleClose}
                scroll={scroll}
                aria-labelledby="scroll-dialog-title"
                aria-describedby="scroll-dialog-description"
            >
                <DialogTitle id="scroll-dialog-title">Create Debit Claim</DialogTitle>
                <DialogContent dividers={scroll === 'paper'}>
                    <DialogTitle id="scroll-dialog-title">Transaction Information</DialogTitle>
                    <TextField
                        fullWidth
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Tanggal Transaksi"
                        defaultValue="2019-01-01"
                        className={classes.textField}
                        margin="dense"
                    />
                    <TextField
                        fullWidth
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Waktu Transaksi"
                        defaultValue="21:00:09"
                        className={classes.textField}
                        margin="dense"
                    />
                    <TextField
                        fullWidth
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Nomor Kartu"
                        defaultValue="201909438509458"
                        className={classes.textField}
                        margin="dense"
                    />
                    <TextField
                        fullWidth
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Nomor Transaksi"
                        defaultValue="200,900.00"
                        className={classes.textField}
                        margin="dense"
                    />
                    <TextField
                        fullWidth
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Status Transaksi"
                        defaultValue="Berhasil"
                        className={classes.textField}
                        margin="dense"
                    />
                    <TextField
                        fullWidth
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Kode Terminal"
                        defaultValue="103924789"
                        className={classes.textField}
                        margin="dense"
                    />
                    <TextField
                        fullWidth
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Kode Pemrosesan"
                        defaultValue="0000000000"
                        className={classes.textField}
                        margin="dense"
                    />
                    <TextField
                        fullWidth
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Kode Respons"
                        defaultValue="00"
                        className={classes.textField}
                        margin="dense"
                    />
                    <TextField
                        fullWidth
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Issuer"
                        defaultValue="MDR"
                        className={classes.textField}
                        margin="dense"
                    />
                    <TextField
                        fullWidth
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Acquirer"
                        defaultValue="BRI"
                        className={classes.textField}
                        margin="dense"
                    />
                    <TextField
                        fullWidth
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Nama Merchant"
                        defaultValue="Mitra Adira Utama"
                        className={classes.textField}
                        margin="dense"
                    />
                    <TextField
                        fullWidth
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Lokasi Merchant"
                        defaultValue="Pergudangan CID 87"
                        className={classes.textField}
                        margin="dense"
                    />
                    <TextField
                        fullWidth
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Nomor Referensi"
                        defaultValue="09897876867868"
                        className={classes.textField}
                        margin="dense"
                    />
                    <TextField
                        fullWidth
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Status Klaim"
                        defaultValue="Claimable"
                        className={classes.textField}
                        margin="dense"
                    />
                    <DialogTitle id="scroll-dialog-title">Informasi Claim Terakhir</DialogTitle>
                    <TextField
                        style={{ margin: '0.5rem' }}
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Tanggal Claim"
                        defaultValue="2019-01-01"
                        className={classes.textField}
                        margin="dense"
                    />
                    <TextField
                        style={{ margin: '0.5rem' }}
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="No Registrasi Claim"
                        defaultValue="FCB20987657435"
                        className={classes.textField}
                        margin="dense"
                    />
                    <TextField
                        style={{ margin: '0.5rem' }}
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Jenis Nominal Claim"
                        defaultValue="Full Amount"
                        className={classes.textField}
                        margin="dense"
                    />
                    <TextField
                        style={{ margin: '0.5rem' }}
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Nominal Claim"
                        defaultValue="409,098.00"
                        className={classes.textField}
                        margin="dense"
                    />
                    <TextField
                        style={{ margin: '0.5rem' }}
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Reason Code"
                        defaultValue="2019-Retrival request tidak di penuhi dalam batas waktu yang ditentukan"
                        className={classes.textField}
                        margin="dense"
                    />
                    <TextField
                        style={{ margin: '0.5rem' }}
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Status Claim"
                        defaultValue="New 2019-09-09 operator@mmdk.com"
                        className={classes.textField}
                        margin="dense"
                    />
                    <TextField
                        style={{ margin: '0.5rem' }}
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Pembuat Claim"
                        defaultValue="MDR"
                        className={classes.textField}
                        margin="dense"
                    />
                    <TextField
                        style={{ margin: '0.5rem' }}
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Penerima Claim"
                        defaultValue="BRI"
                        className={classes.textField}
                        margin="dense"
                    />
                    <TextField
                        disabled
                        id="outlined-multiline-static"
                        label="Deskripsi"
                        multiline
                        defaultValue=""
                        fullWidth
                        rows="4"
                        className={classes.textField}
                        margin="normal"
                        variant="outlined"
                    />
                    <DialogTitle id="scroll-dialog-title">Input Claim</DialogTitle>
                    <TextField
                        label="Tanggal Claim"
                        placeholder="Tanggal Claim"
                        className={classes.TextField}
                        variant="outlined"
                        fullWidth
                        margin="dense"
                        id="date"
                        type="date"
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
                    <TextField
                        id="outlined-select-currency"
                        select
                        fullWidth
                        label="Reason Code"
                        className={classes.textField}
                        value={currency}
                        onChange={handleChange}
                        SelectProps={{
                            MenuProps: {
                                className: classes.menu,
                            },
                        }}
                        margin="dense"
                        variant="outlined"
                    >
                        {currencies.map(option => (
                            <MenuItem key={option.value} value={option.value}>
                                {option.label}
                            </MenuItem>
                        ))}
                    </TextField>
                    <TextField
                        label="No Registrasi Claim"
                        placeholder="No Registrasi Claim"
                        className={classes.TextField}
                        variant="outlined"
                        fullWidth
                        margin="dense"
                    />
                    <TextField
                        disabled
                        label="Jenis Claim"
                        placeholder="Jenis Claim"
                        className={classes.TextField}
                        variant="outlined"
                        defaultValue="Chargeback"
                        fullWidth
                        margin="dense"
                    />

                    <TextField
                        fullWidth
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Status Claim"
                        defaultValue="New"
                        className={classes.textField}
                        margin="dense"
                    />
                    <FormControl component="fieldset" className={classes.formControl}>
                        <FormLabel component="legend">Jenis Nominal Claim</FormLabel>
                        <RadioGroup aria-label="gender" name="gender1" value={value} onChange={handleChangeRadio}>
                            <FormControlLabel value="fullamount" control={<Radio />} label="Full Amount" />
                            <FormControlLabel value="partialamount" control={<Radio />} label="Partial Amount" />
                        </RadioGroup>
                    </FormControl>
                    <TextField
                        disabled
                        label="Nominal Claim"
                        placeholder="Nominal Claim"
                        className={classes.TextField}
                        variant="outlined"
                        fullWidth
                        defaultValue="283,764.00"
                        margin="dense"
                    />
                    <TextField
                        id="outlined-multiline-static"
                        label="Deskripsi"
                        multiline
                        fullWidth
                        rows="4"
                        className={classes.textField}
                        margin="normal"
                        variant="outlined"
                    />
                    <TextField
                        fullWidth
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Pembuat Claim"
                        defaultValue="MDR"
                        className={classes.textField}
                        margin="dense"
                    />
                    <TextField
                        fullWidth
                        disabled
                        variant="outlined"
                        id="standard-disabled"
                        label="Penerima Claim"
                        defaultValue="BRI"
                        className={classes.textField}
                        margin="dense"
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClickOpenRequest} variant="contained" size="small" color="primary" className={classes.margin}>
                        save
                    </Button>
                    <Button href="/admin/viewdebitclaim" variant="contained" size="small" color="primary" className={classes.margin}>
                        view & edit
                    </Button>
                    <Button onClick={handleClickOpenRequest} variant="contained" size="small" color="primary" className={classes.margin}>
                        submit for approval
                    </Button>
                    <Button onClick={handleClose} variant="contained" size="small" color="primary" className={classes.margin}>
                        cancel
                    </Button>
                </DialogActions>
            </Dialog>
            <Dialog
                open={openRequest}
                onClose={handleCloseRequest}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">{"Request Submited"}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        <FormLabel >Submite For Approval Chargeback Rev :</FormLabel>
                        <TextField
                            fullWidth
                            disabled
                            variant="outlined"
                            id="standard-disabled"
                            defaultValue="FCR129328.doc"
                            className={classes.textField}
                            margin="dense"
                        />
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseRequest} color="primary" autoFocus>
                        Continue
                    </Button>
                </DialogActions>
            </Dialog>
        </div>

    );
}
