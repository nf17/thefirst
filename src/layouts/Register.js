
import React from "react";
import RegisterPage from "views/Register/Register.js";


export default function Register({ ...rest }) {
    return (
        <RegisterPage rest={rest} />
    );
}
