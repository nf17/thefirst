
import React from "react";
import LoginPage from "views/Login/Login.js";


export default function Login({ ...rest }) {
    return (
        <LoginPage rest={rest} />
    );
}
