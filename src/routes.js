
import Notifications from "@material-ui/icons/Notifications";
// core components/views for Admin layout
import DashboardPage from "views/Dashboard/Dashboard.js";
import UserProfile from "views/UserProfile/UserProfile.js";
import TableList from "views/TableList/TableList.js";
import Typography from "views/Typography/Typography.js";
import Icons from "views/Icons/Icons.js";
import Maps from "views/Maps/Maps.js";
import NotificationsPage from "views/Notifications/Notifications.js";
import LoginPage from "views/Login/Login.js";
import TabsTransaction from "components/Tabs/TabsPage.js";
import ListDebitClaim from "components/Tabs/TabsPageClaim.js"
// core components/views for RTL layout
//icon material
import AccountBalanceWalletIcon from '@material-ui/icons/AccountBalanceWallet';
import HomeIcon from '@material-ui/icons/Home';
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';
import BarChartIcon from '@material-ui/icons/BarChart';
import PermContactCalendarIcon from '@material-ui/icons/PermContactCalendar';
import EventIcon from '@material-ui/icons/Event';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import PanToolIcon from '@material-ui/icons/PanTool';

const dashboardRoutes = [

  {
    path: "/dashboard",
    name: "HomePage",
    icon: HomeIcon,
    component: DashboardPage,
    layout: "/admin"
  },
  {
    path: "/transaction",
    name: "Transaction",
    icon: AccountBalanceWalletIcon,
    component: TabsTransaction,
    layout: "/admin"
  },
  {
    path: "/claim",
    name: "Claim",
    icon: PanToolIcon,
    component: ListDebitClaim,
    layout: "/admin"
  },
  {
    path: "/download",
    name: "Download Report",
    icon: "content_paste",
    component: CloudDownloadIcon,
    layout: "/admin"
  },
  {
    path: "/activity",
    name: "Activity Log",
    icon: BarChartIcon,
    component: Typography,
    layout: "/admin"
  },
  {
    path: "/contact",
    name: "Contact Person",
    icon: PermContactCalendarIcon,
    component: Icons,
    layout: "/admin"
  },
  {
    path: "/calender",
    name: "View Calender",
    icon: EventIcon,
    component: Maps,
    layout: "/admin"
  },
  {
    path: "/login",
    name: "Logout",
    rtlName: "Login",
    icon: ExitToAppIcon,
    component: LoginPage,
    layout: "/Login"
  }
];

export default dashboardRoutes;
