FROM node:10

MAINTAINER Papay Baron "ferdinand.anton@ogya.co.id"

WORKDIR /usr/src/web-reactjs

COPY package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm ci --only=production
# Bundle app source

COPY . .

EXPOSE 3000
CMD [ "npm", "start" ]